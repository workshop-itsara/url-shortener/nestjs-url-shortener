import { Controller, Get, Param, Response } from '@nestjs/common';
import { UrlShortenerService } from './module/url-shortener/url-shortener.service';

@Controller()
export class AppController {
    constructor(private readonly urlShortenerService: UrlShortenerService) { }

    @Get('/:shortUrl')
    async getLongUrl(
        @Param('shortUrl') shortUrl: string,
        @Response() res: any
    ) {
        let longUrl: string = await this.urlShortenerService.getLongUrl(shortUrl);
        res.redirect(longUrl);
    }
}