import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ConfigService } from '@nestjs/config';
import { Not, Repository } from 'typeorm';
import { CreateUrlShortenerDto, UpdateUrlShortenerDto } from './dto';
import { UrlShortener } from './entity';
import { ObjectId } from 'mongodb';
import * as moment from 'moment';
import * as randomString from 'randomstring';

@Injectable()
export class UrlShortenerService {
    constructor(
        private configService: ConfigService,
        @InjectRepository(UrlShortener) private readonly urlShortenerRepository: Repository<UrlShortener>
    ) { }

    async getUrlShortener(): Promise<object[]> {
        const urlShortener: UrlShortener[] = await this.urlShortenerRepository.find({
            select: [
                `_id`,
                `long_url`,
                `short_url`,
                `count`
            ],
            order: { updated_at: `DESC` }
        });

        urlShortener.forEach((data) => data.short_url = `${this.configService.get('APP_PATH')}/${data.short_url}`);
        return urlShortener;
    }

    async getUrlShortenerDetail(id: string): Promise<object> {
        const urlShortener: UrlShortener = await this.urlShortenerRepository.findOne({
            select: [
                `_id`,
                `long_url`,
                `short_url`,
                `count`
            ],
            where: {
                _id: new ObjectId(id)
            }
        });

        if (!urlShortener) throw new HttpException('ไม่พบข้อมูล', HttpStatus.NOT_FOUND);
        urlShortener.short_url = `${this.configService.get('APP_PATH')}/${urlShortener.short_url}`
        return urlShortener;
    }

    async getLongUrl(shortUrl: string): Promise<string> {
        const urlShortener: UrlShortener = await this.urlShortenerRepository.findOne({
            select: [
                `long_url`,
                `count`
            ],
            where: {
                short_url: shortUrl
            }
        });

        if (!urlShortener) throw new HttpException('ไม่พบหน้า', HttpStatus.NOT_FOUND);
        urlShortener['count']++;
        await this.urlShortenerRepository.save(urlShortener);
        return urlShortener['long_url'];
    }

    async createUrlShortener(createUrlShortenerDto: object): Promise<object> {
        const longUrl: UrlShortener = await this.urlShortenerRepository.findOneBy({ long_url: createUrlShortenerDto['long_url'] });
        if (longUrl) throw new HttpException('Long URL นี้เคยถูกตั้งค่าไว้แล้ว', HttpStatus.BAD_REQUEST);
        const urlShortener: UrlShortener = new UrlShortener();
        urlShortener['long_url'] = createUrlShortenerDto['long_url'];
        urlShortener['short_url'] = randomString.generate({ length: 5, charset: 'alphabetic' });
        urlShortener['count'] = 0;
        await this.urlShortenerRepository.save(urlShortener);
        return {
            message: 'Url Shortener created'
        };
    }

    async updateUrlShortener(id: string, updateUrlShortenerDto: object): Promise<object> {
        const longUrl: UrlShortener = await this.urlShortenerRepository.findOneBy({
            _id: Not(new ObjectId(id)),
            long_url: updateUrlShortenerDto['long_url']
        });

        if (longUrl) throw new HttpException('Long URL นี้เคยถูกตั้งค่าไว้แล้ว', HttpStatus.BAD_REQUEST);
        const urlShortener: UrlShortener = await this.urlShortenerRepository.findOneBy({ _id: new ObjectId(id) });
        if (!urlShortener) throw new HttpException('ไม่พบข้อมูล', HttpStatus.NOT_FOUND);
        if (urlShortener['long_url'] !== updateUrlShortenerDto['long_url']) urlShortener['short_url'] = randomString.generate({ length: 5, charset: 'alphabetic' });
        urlShortener['long_url'] = updateUrlShortenerDto['long_url'];
        urlShortener['count'] = 0;
        urlShortener['updated_at'] = moment().format('YYYY-MM-DD HH:mm:ss');
        await this.urlShortenerRepository.save(urlShortener);
        return {
            message: 'Url Shortener updated'
        };
    }

    async deleteUrlShortener(id: string): Promise<object> {
        const urlShortener: UrlShortener = await this.urlShortenerRepository.findOneBy({ _id: new ObjectId(id) });
        if (!urlShortener) throw new HttpException('ไม่พบข้อมูล', HttpStatus.NOT_FOUND);
        await this.urlShortenerRepository.delete({ _id: new ObjectId(id) });
        return {
            message: 'Url Shortener deleted'
        };
    }
}
