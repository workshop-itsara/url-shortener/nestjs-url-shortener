import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UrlShortenerService } from './url-shortener.service';
import { UrlShortenerController } from './url-shortener.controller';
import { UrlShortener } from './entity';

@Module({
    imports: [TypeOrmModule.forFeature([UrlShortener])],
    providers: [UrlShortenerService],
    controllers: [UrlShortenerController],
    exports: [UrlShortenerService]
})
export class UrlShortenerModule { }
