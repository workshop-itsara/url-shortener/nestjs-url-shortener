import { CreateUrlShortenerDto } from './create-url-shortener.dto';
import { UpdateUrlShortenerDto } from './update-url-shortener.dto';

export {
    CreateUrlShortenerDto,
    UpdateUrlShortenerDto
};