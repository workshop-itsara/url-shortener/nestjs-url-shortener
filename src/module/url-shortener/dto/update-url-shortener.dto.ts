import { IsNotEmpty, IsOptional } from 'class-validator';

export class UpdateUrlShortenerDto {
    @IsOptional()
    @IsNotEmpty()
    long_url: string;
}