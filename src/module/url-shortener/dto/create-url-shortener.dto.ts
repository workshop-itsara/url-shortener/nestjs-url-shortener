import { IsNotEmpty } from 'class-validator';

export class CreateUrlShortenerDto {
    @IsNotEmpty()
    long_url: string;
}