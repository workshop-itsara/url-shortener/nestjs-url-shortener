import { Column, Entity, ObjectIdColumn } from 'typeorm';
import { ObjectId } from 'mongodb';

@Entity({ name: 'url_shortener' })
export class UrlShortener {
    @ObjectIdColumn({ comment: 'ID' })
    _id: ObjectId;

    @Column({ type: 'text', comment: 'URL แบบเต็ม' })
    long_url: string;

    @Column({ type: 'text', comment: 'URL แบบย่อ' })
    short_url: string;

    @Column({ type: 'int', default: 0, comment: 'จำนวนครั้งที่เข้าชม' })
    count: number;

    @Column({ type: 'datetime', default: () => 'CURRENT_TIMESTAMP', comment: 'วันที่แก้ไข' })
    updated_at: Date | String;
}