import { Body, Controller, Delete, Get, HttpCode, HttpException, HttpStatus, Param, Patch, Post, Put, Query, Request, Response } from '@nestjs/common';
import { UrlShortenerService } from './url-shortener.service';
import { CreateUrlShortenerDto, UpdateUrlShortenerDto } from './dto';

@Controller('url-shortener')
export class UrlShortenerController {
    constructor(private readonly urlShortenerService: UrlShortenerService) { }

    @Get('get-url-shortener')
    async getUrlShortener(): Promise<object[]> {
        return await this.urlShortenerService.getUrlShortener();
    }

    @Get('get-url-shortener-detail/:id')
    async getUrlShortenerDetail(@Param('id') id: string): Promise<object> {
        return await this.urlShortenerService.getUrlShortenerDetail(id);
    }

    @Post('create-url-shortener')
    async createUrlShortener(@Body() createUrlShortenerDto: CreateUrlShortenerDto): Promise<object> {
        return await this.urlShortenerService.createUrlShortener(createUrlShortenerDto);
    }

    @Patch('update-url-shortener/:id')
    async updateUrlShortener(
        @Param('id') id: string,
        @Body() updateUrlShortenerDto: UpdateUrlShortenerDto
    ): Promise<object> {
        return await this.urlShortenerService.updateUrlShortener(id, updateUrlShortenerDto);
    }

    @Delete('delete-url-shortener/:id')
    async deleteUrlShortener(@Param('id') id: string): Promise<object> {
        return await this.urlShortenerService.deleteUrlShortener(id);
    }
}