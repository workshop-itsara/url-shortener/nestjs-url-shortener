import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TypeOrmConfigService } from './config/typeorm-config.service';
import { UrlShortenerModule } from './module/url-shortener/url-shortener.module';
import { AppController } from './app.controller';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: `env/${(!process.env.NODE_ENV ? 'development' : process.env.NODE_ENV)}.env`
    }),
    TypeOrmModule.forRootAsync({
      inject: [TypeOrmConfigService],
      useClass: TypeOrmConfigService
    }),
    UrlShortenerModule
  ],
  controllers: [AppController]
})
export class AppModule { }
