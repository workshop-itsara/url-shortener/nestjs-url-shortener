import { NestFactory } from '@nestjs/core';
import { ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { AppModule } from './app.module';
import * as cookieParser from 'cookie-parser';
import * as cors from 'cors';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const configService = app.get(ConfigService);
  app.use(cors({
    origin: true,
    credentials: true
  }));
  app.use(cookieParser());
  app.useGlobalPipes(new ValidationPipe());
  await app.listen(configService.get('APP_PORT'));
  console.log('APP_MODE:', configService.get('APP_MODE'), '| NODE_ENV:', process.env.NODE_ENV);
}
bootstrap();
