<div align="center">
    <img src="https://www.vectorlogo.zone/logos/nestjs/nestjs-ar21.svg" width="200">
</div>

# NestJS - URL Shortener

API for build a URL shortener

## Table of Contents

- [Prerequisites](#prerequisites)
- [Configuration](#configuration)
- [Installation](#installation)
- [Usage](#usage)
- [Credits](#credits)
- [License](#license)

## Prerequisites

* NestJS : 8.0.0
* Node : 16.15.0
* Package Manager (npm) : 8.5.5

## Configuration

* Port : 3000

## Installation

Using npm:

```bash
npm install
```

Using docker:

```bash
docker compose up -d
```

## Usage

Using npm:

```bash
npm run start:dev
```

Using docker:

```bash
docker ps --filter name=nestjs_url_shortener
```

## Credits

Itsara Rakchanthuek

## License

[MIT](LICENSE)