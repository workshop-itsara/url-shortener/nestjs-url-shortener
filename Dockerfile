FROM node:14-alpine AS development
WORKDIR /usr/src/app
COPY package*.json .
RUN npm i
COPY . .

FROM node:14-alpine AS build
WORKDIR /usr/src/app
COPY package*.json .
COPY --from=development /usr/src/app/node_modules ./node_modules
COPY . .
RUN npm run build

FROM node:14-alpine AS production
COPY --from=build /usr/src/app/node_modules ./node_modules
COPY --from=build /usr/src/app/dist ./dist
COPY . .
CMD ["npm", "run", "start:prod"]