# 1. System Design
### *จงออกแบบระบบย่อ URL พร้อม demo เช่น ย่อ http://my-order.ai/long-url/very-sub-path ให้เป็น http://shorturl.com/abcde*

> ### **การออกแบบระบบ**
1. รับค่า URL แบบเต็มจากผู้ใช้
2. สุ่ม Code 5 ตัวอักษรสำหรับใช้ Ref แทน URL แบบเต็ม แล้วเก็บเข้า DB
3. ส่ง URL แบบย่อกลับไปให้ผู้ใช้
4. เวลาผู้ใช้คลิกที่ลิ้งค์ URL แบบย่อ ระบบจะไปเช็คข้อมูลใน DB โดยเช็คจาก Code ของลิ้งค์ที่ Ref กันอยู่ ถ้าพบข้อมูลจะ Redirect ไปที่ URL แบบเต็มให้ และมีการนับจำนวน Visited ไว้ด้วย

> ### **Stack ที่ใช้**
* **Front-end** - เลือกใช้เป็น Angular เพราะมีโครงสร้างที่เข้าใจง่าย วาง Directory เป็นหมวดหมู่ชัดเจน และเป็น OOP และใช้ TypeScript ทำให้โค้ดร่วมกับในทีมง่าย และมี CLI ช่วยให้สะดวกขึ้น
* **Back-end** - เลือกใช้เป็น Node.js โดยใช้ NestJS เป็น Framework ที่เลือกเพราะมีโครงสร้างเป็นแบบ OOP แบบเดียวกับ Angular เป็น OOP ใช้ TypeScript มี CLI ช่วย และเหมาะสมกับการ Scale
* **Database** - เลือกใช้เป็น MongoDB เพราะเก็บข้อมูลง่าย ยืดหยุ่น เห็นว่าทาง MyOrder ใช้ MongoDB ด้วยเลยอยากลองใช้ดูครับ
* **Deployment**
    * **Front-end** - เลือกใช้ Netlify เป็น Host ชั่วคราวให้ทาง MyOrder ใช้ทดสอบระบบครับ
    * **Back-end** - ปกติการ Deploy จะ Build บน Host แล้ว Point Domain ครับ แต่ผมไม่ได้เช่า Host ไว้ ถ้าทาง MyOrder จะทดสอบให้ Clone Git ของฝั่ง Back-end ลงและจำลองเครื่องตัวเองเป็น Host แทนจะใช้งานได้ครับ

> ### **Link สำหรับทดสอบ**
* [https://myorder-url-shortener.netlify.app](https://myorder-url-shortener.netlify.app)

> **ถ้าทาง MyOrder จะทดสอบระบบ ให้ผมเทสให้ดูผ่านทาง Video Conference ในวันสัมภาษณ์ได้นะครับ**